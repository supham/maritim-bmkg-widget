/*
<?php
    $id = $_GET['id'] ?? null;

    if (!$id) {
        throw new UnexpectedValueException('parameter `id` required');
    }

    $code = $_GET['code'] ?? null;
    if (!$code) {
        throw new UnexpectedValueException('parameter `code` required');
    }
    $data_pelabuhan = require(__DIR__.'/pelabuhan.php');
    foreach ($data_pelabuhan as $pelabuhan) {
        if ($pelabuhan[0] === $code) {
            $pelabuhan = (object)['code' => $code, 'nama' => $pelabuhan[1]];
            break;
        }
    }
?>
*/
(function() {
    let script
    if (!window.wretch) {
        script = document.createElement('script')
        script.src = 'https://unpkg.com/wretch'
        document.head.append(script)
    }
    if (!window.flatpickr) {
        script = document.createElement('script')
        script.src = 'https://unpkg.com/flatpickr'
        document.head.append(script)
        script = document.createElement('script')
        script.src = 'https://unpkg.com/browse/flatpickr/dist/l10n/id.js'
        document.head.append(script)
    }

    document.querySelector('#<?= $id ?>').setAttribute('style', "padding: 0px; margin: 1px 0px 0px; font-family: Arial, Helvetica, sans-serif; width: 200px; border-radius: 1px; text-align: center; background-color: rgb(255, 255, 255); border: 1px solid rgb(30, 136, 229); line-height: 14px;");

    function renderItem(data) {
        let tanggal = flatpickr.formatDate(new Date(data.valid_from), 'd M, H:i') +' s/d '+ flatpickr.formatDate(new Date(data.valid_to), 'd M, H:i')
        let suhu = (data.temp_min > 0 ? '+' :'')+ (data.temp_min - 0) +'°C s/d '+ (data.temp_max > 0 ? '+' :'') + (data.temp_max - 0) +'°C'

        return `<div style=";padding:0;margin:0;padding:5px;padding-top:8px;background-color:#FFFFFF00;">
                <div style=";padding:0;margin:0;color:grey;font-size:11px;padding-bottom:2px;">${tanggal}</div>
                <div style=";padding:0;margin:0;color:grey;font-size:11px;padding-bottom:2px;">${data.weather}</div>
                <div style=";padding:0;margin:0;height:45px;width:45px;display:inline-block;margin-right:10px;">
                    <img src="https://cuacalab.id/assets/img/ic/dlr.svg" style="height:45px;width:45px;display:inline-block;">
                </div>
                <span style=";padding:0;margin:0;font-size:20px;vertical-align:top;margin-top:15px;display:inline-block;color:#000000;">${suhu}</span>
            </div>`
    }

    function render(json) {
        document.querySelector('#<?= $id ?>').innerHTML = `
            <div id="ml_6ef53e0_c" style="padding:0;margin:0;padding:7px 5px;">
                <div style=";padding:0;margin:0;padding:7px 5px;padding-bottom:8px;font-weight:bold;font-size:14px;background-color:#1E88E5;color:#FFFFFF;"><?= $pelabuhan->nama ?></div>
                    ${renderItem(json.data[1])}<hr>
                    ${renderItem(json.data[0])}
                </div>
            </div>
            <div id="ml_qqqq_c" style="padding:0;margin:0;padding:7px 5px;">
                <img src="https://cuacalab.id/assets/img/logo_z_b.svg" style="width:15px;opacity:0.7;margin-right:5px;position:relative;top:1px;display:inline-block;">
                <a href="https://cuacalab.id/cuaca_bandung/hari_ini/" style="color:grey;font-size:12px;text-decoration:none;" target="_blank" id="ml_qqqq_u">Cuaca Hari ini</a>
            </div>
        `
    }

    function load() {
        fetch("https://peta-maritim.bmkg.go.id/public_api/pelabuhan/<?= $pelabuhan->code.'_'.$pelabuhan->nama ?>.json", {
            "headers": {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "accept-language": "en-US,en;q=0.9",
                "cache-control": "max-age=0",
                "sec-fetch-dest": "document",
                "sec-fetch-mode": "navigate",
                "sec-fetch-site": "same-origin",
                "sec-fetch-user": "?1",
                "sec-gpc": "1",
                "upgrade-insecure-requests": "1"
            },
            "referrer": "https://peta-maritim.bmkg.go.id/public_api/pelabuhan",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "body": null,
            "method": "GET",
            "mode": "cors",
            "credentials": "omit"
        })
        .then(async res => render(await res.json()))
        .catch(console.error)
    }

    setTimeout(load, 1000)
})();
