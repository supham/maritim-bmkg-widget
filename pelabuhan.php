<?php return [
    [
        "0001",
        "Belawan"
    ],
    [
        "0002",
        "Teluk Bayur"
    ],
    [
        "0003",
        "Panjang"
    ],
    [
        "0004",
        "Tanjung Priok"
    ],
    [
        "0005",
        "Tanjung Intan"
    ],
    [
        "0006",
        "Tanjung Mas"
    ],
    [
        "0007",
        "Tanjung Perak"
    ],
    [
        "0008",
        "Tenau"
    ],
    [
        "0009",
        "Dwikora"
    ],
    [
        "0010",
        "Soekarno - Hatta"
    ],
    [
        "0011",
        "Kendari"
    ],
    [
        "0012",
        "Samudera"
    ],
    [
        "0013",
        "Biak"
    ],
    [
        "0014",
        "Batu Ampar"
    ],
    [
        "0015",
        "Yos Sudarso"
    ],
    [
        "0016",
        "Malahayati"
    ],
    [
        "0023",
        "Sinabang"
    ],
    [
        "0026",
        "Sibolga"
    ],
    [
        "0027",
        "Gunung Sitoli"
    ],
    [
        "0028",
        "Teluk Dalam"
    ],
    [
        "0047",
        "Tanjung Balai Asahan"
    ],
    [
        "0082",
        "Ferry Harbour Bay"
    ],
    [
        "0084",
        "Sekupang"
    ],
    [
        "0085",
        "Tarempa"
    ],
    [
        "0088",
        "Sintete"
    ],
    [
        "0089",
        "Sukabangun"
    ],
    [
        "0099",
        "Tanjung Pandan"
    ],
    [
        "0101",
        "Sunda Kelapa"
    ],
    [
        "0102",
        "Marunda"
    ],
    [
        "0104",
        "Kalibaru"
    ],
    [
        "0105",
        "Muara Angke"
    ],
    [
        "0106",
        "PPS Muara Baru"
    ],
    [
        "0107",
        "Cirebon"
    ],
    [
        "0114",
        "PP Labuan"
    ],
    [
        "0115",
        "PP Karangantu"
    ],
    [
        "0116",
        "Bojonegara"
    ],
    [
        "0119",
        "Kota Agung"
    ],
    [
        "0121",
        "Mesuji"
    ],
    [
        "0128",
        "Jepara"
    ],
    [
        "0129",
        "Karimun Jawa"
    ],
    [
        "0142",
        "Kumai"
    ],
    [
        "0151",
        "Gresik"
    ],
    [
        "0154",
        "Kalianget"
    ],
    [
        "0164",
        "ASDP Ketapang"
    ],
    [
        "0165",
        "Benoa, Bali"
    ],
    [
        "0167",
        "Padangbai"
    ],
    [
        "0168",
        "Celukan Bawang"
    ],
    [
        "0170",
        "ASDP Gilimanuk"
    ],
    [
        "0172",
        "Lembar"
    ],
    [
        "0186",
        "Rote"
    ],
    [
        "0193",
        "Semayang, Balikpapan"
    ],
    [
        "0194",
        "Samarinda"
    ],
    [
        "0195",
        "Tarakan"
    ],
    [
        "0198",
        "Tanjung Santan"
    ],
    [
        "0199",
        "Sangkuliran"
    ],
    [
        "0200",
        "Lok Tuan"
    ],
    [
        "0207",
        "Pare-pare"
    ],
    [
        "0215",
        "Bajoe"
    ],
    [
        "0219",
        "Biringkasi"
    ],
    [
        "0224",
        "Salakan"
    ],
    [
        "0225",
        "Kolondale"
    ],
    [
        "0229",
        "Raha"
    ],
    [
        "0231",
        "Langara"
    ],
    [
        "0232",
        "Bau Bau"
    ],
    [
        "0237",
        "Nunukan"
    ],
    [
        "0243",
        "Poso"
    ],
    [
        "0245",
        "Gorontalo"
    ],
    [
        "0246",
        "PPN Kwandang"
    ],
    [
        "0250",
        "Manado"
    ],
    [
        "0251",
        "Tahuna"
    ],
    [
        "0252",
        "Lirung"
    ],
    [
        "0257",
        "Ahmad Yani, Ternate"
    ],
    [
        "0267",
        "Banda Neira"
    ],
    [
        "0268",
        "Namlea"
    ],
    [
        "0269",
        "Amahai"
    ],
    [
        "0270",
        "Tulehu"
    ],
    [
        "0271",
        "Tual"
    ],
    [
        "0272",
        "Dobo"
    ],
    [
        "0276",
        "Waipirit"
    ],
    [
        "0278",
        "Jayapura"
    ],
    [
        "0280",
        "Serui"
    ],
    [
        "0281",
        "Nabire"
    ],
    [
        "0284",
        "Manokwari"
    ],
    [
        "0285",
        "Wasior"
    ],
    [
        "0286",
        "Merauke"
    ],
    [
        "0291",
        "Sorong"
    ],
    [
        "0292",
        "Fak-fak"
    ],
    [
        "0297",
        "Kokas"
    ],
    [
        "0301",
        "PPS Belawan"
    ],
    [
        "0309",
        "PPN Pemangkat"
    ],
    [
        "0310",
        "PPP Sungai Rengas"
    ],
    [
        "0311",
        "PPP Teluk Batang"
    ],
    [
        "0313",
        "PPN Tanjung Pandan"
    ],
    [
        "0315",
        "PPN Kejawanan"
    ],
    [
        "0325",
        "PPP Labuhan Maringgai"
    ],
    [
        "0326",
        "PPP Lempasing"
    ],
    [
        "0328",
        "PPP Teladas"
    ],
    [
        "0329",
        "PPN Pekalongan"
    ],
    [
        "0331",
        "PPP Bajomulyo"
    ],
    [
        "0333",
        "Morodemak"
    ],
    [
        "0334",
        "PPP Tasik Agung"
    ],
    [
        "0335",
        "PPP Tawang"
    ],
    [
        "0339",
        "PP Banjarmasin"
    ],
    [
        "0340",
        "PPS Cilacap"
    ],
    [
        "0350",
        "PPN Pengambengan"
    ],
    [
        "0353",
        "PPS Kendari"
    ],
    [
        "0356",
        "PPS Bitung"
    ],
    [
        "0359",
        "PPN Ternate"
    ],
    [
        "0361",
        "PPN Ambon"
    ],
    [
        "0370",
        "ASDP Merak"
    ],
    [
        "0371",
        "ASDP Bakauheni"
    ],
    [
        "0377",
        "Teluk Bungus"
    ],
    [
        "0379",
        "Tua Pejat"
    ],
    [
        "0380",
        "Tanjung Api Api"
    ],
    [
        "0382",
        "Tanjung Kelian"
    ],
    [
        "0383",
        "Tanjung Batu"
    ],
    [
        "0387",
        "Garongkong"
    ],
    [
        "0389",
        "Tebas Kuala"
    ],
    [
        "0391",
        "Sangkapura"
    ],
    [
        "0398",
        "Bolok"
    ],
    [
        "0404",
        "Tanjung Bira"
    ],
    [
        "0405",
        "Pamamata"
    ],
    [
        "0407",
        "Sikeli"
    ],
    [
        "0409",
        "Kolaka"
    ],
    [
        "0412",
        "Torobulu"
    ],
    [
        "0413",
        "Tampo"
    ],
    [
        "0414",
        "Dongkala"
    ],
    [
        "0417",
        "Luwuk"
    ],
    [
        "0418",
        "Penajam"
    ],
    [
        "0419",
        "Karingau"
    ],
    [
        "0423",
        "Wanci"
    ],
    [
        "0427",
        "PPN Donggala"
    ],
    [
        "0428",
        "Klademak"
    ],
    [
        "0429",
        "Pantoloan"
    ],
    [
        "0430",
        "Pondong"
    ],
    [
        "0431",
        "Bastiong"
    ],
    [
        "0432",
        "Kayoa"
    ],
    [
        "0433",
        "Sidangole"
    ],
    [
        "0434",
        "Rum"
    ],
    [
        "0435",
        "Sofifi"
    ],
    [
        "0437",
        "Tobelo"
    ],
    [
        "0438",
        "Babang"
    ],
    [
        "0439",
        "Batang Dua"
    ],
    [
        "0441",
        "Ferry Batam Centre International"
    ],
    [
        "0443",
        "Subaim"
    ],
    [
        "0444",
        "Waode Buri"
    ],
    [
        "0446",
        "Pulau Moti"
    ],
    [
        "0448",
        "Daruba-Morotai"
    ],
    [
        "0449",
        "Pulau Makian"
    ],
    [
        "0450",
        "Krui"
    ],
    [
        "0451",
        "Waesai"
    ],
    [
        "0452",
        "Arar"
    ],
    [
        "0455",
        "Pelayaran Rakyat Sorong"
    ],
    [
        "0456",
        "Dermaga Usaha Mina"
    ],
    [
        "0457",
        "Sausapor"
    ],
    [
        "0458",
        "Babo"
    ],
    [
        "0459",
        "Misool"
    ],
    [
        "0463",
        "Amolengo"
    ],
    [
        "0464",
        "Amurang"
    ],
    [
        "0465",
        "Likupang"
    ],
    [
        "0469",
        "Penagi"
    ],
    [
        "0470",
        "Sampit"
    ],
    [
        "0471",
        "Letung"
    ],
    [
        "0472",
        "Kuala Maras"
    ],
    [
        "0473",
        "Pulau Laut"
    ],
    [
        "0474",
        "Sedanau"
    ],
    [
        "0475",
        "Pulau Tiga"
    ],
    [
        "0476",
        "Midai"
    ],
    [
        "0477",
        "Subi"
    ],
    [
        "0478",
        "Serasan"
    ],
    [
        "0479",
        "Rasau Jay"
    ],
    [
        "0480",
        "Ciwandan"
    ],
    [
        "0481",
        "Pelabuhan ANyer"
    ],
    [
        "0483",
        "Pelabuhan di DKI Jakartaa"
    ],
    [
        "0485",
        "Indah Kiat"
    ],
    [
        "0486",
        "TPI Panimbang"
    ],
    [
        "0487",
        "PPI Binuangen"
    ],
    [
        "0488",
        "PP Pasauran"
    ],
    [
        "0489",
        "PP Sumur"
    ],
    [
        "0490",
        "TPI Ie Meulle"
    ],
    [
        "0491",
        "Kuala Tanjung"
    ],
    [
        "0492",
        "Idi"
    ],
    [
        "0493",
        "Calang"
    ],
    [
        "0494",
        "Susoh Blang Pidie"
    ],
    [
        "0495",
        "PPP Carocok"
    ],
    [
        "0496",
        "PPP Klidang Lor"
    ],
    [
        "0497",
        "PP Mayangan"
    ],
    [
        "0498",
        "Tanjung Silopo"
    ],
    [
        "0499",
        "Galesong"
    ],
    [
        "0500",
        "Bangsalae"
    ],
    [
        "0501",
        "Muara Sampara"
    ],
    [
        "0502",
        "PP Muara Kintap"
    ],
    [
        "0504",
        "Patimban"
    ],
    [
        "0505",
        "PPN Merauke"
    ],
    [
        "0401",
        "Lewoleba"
    ],
    [
        "0346",
        "PPP Tamperan"
    ],
    [
        "0524",
        "Jangkar"
    ],
    [
        "0466",
        "Pananaru"
    ],
    [
        "0528",
        "Marina Tanjung Lesung"
    ],
    [
        "0336",
        "PPP Tegalsari"
    ],
    [
        "0529",
        "Padang Tikar"
    ],
    [
        "0530",
        "Bira"
    ],
    [
        "0531",
        "Pamamata, Selayar"
    ],
    [
        "0532",
        "Batulicin"
    ],
    [
        "0533",
        "Cituis Rawa Saban"
    ],
    [
        "0479",
        "Rasau Jaya"
    ],
    [
        "0534",
        "Pelabuhan Internasional Tanjung Balai Karimun"
    ],
    [
        "0534",
        "Internasional Tanjung Balai Karimun"
    ],
    [
        "0535",
        "Parit Rampak"
    ],
    [
        "0536",
        "Khusus Bea Cukai"
    ],
    [
        "0538",
        "Kuala Tungkal"
    ],
    [
        "0537",
        "Talang Duku"
    ],
    [
        "0539",
        "Muara Sabak"
    ],
    [
        "0540",
        "PPI Nipah Panjang"
    ],
    [
        "0538",
        "Roro Kuala Tungkal"
    ],
    [
        "0182",
        "Labuan Bajo"
    ],
    [
        "0541",
        "Taman Nasional Pulau Komodo"
    ],
    [
        "0185",
        "Sabu"
    ],
    [
        "0542",
        "Raijua"
    ],
    [
        "0185",
        "Seba"
    ],
    [
        "0543",
        "Wakai"
    ],
    [
        "0544",
        "Toboli"
    ],
    [
        "0545",
        "Pagimana"
    ],
    [
        "0546",
        "Ampana"
    ],
    [
        "0547",
        "Pasokan"
    ],
    [
        "0548",
        "Dolong"
    ],
    [
        "0549",
        "Marisa"
    ],
    [
        "0550",
        "Ende, Flores"
    ],
    [
        "0551",
        "Maumere"
    ],
    [
        "0552",
        "Hansisi"
    ],
    [
        "0553",
        "Wowong"
    ],
    [
        "0554",
        "Balauring"
    ],
    [
        "0191",
        "Larantuka"
    ],
    [
        "0402",
        "Waiwerang, NTT"
    ],
    [
        "0555",
        "Adonara"
    ],
    [
        "0183",
        "Baranusa, Alor"
    ],
    [
        "0188",
        "Kalabahi"
    ],
    [
        "0189",
        "Waingapu"
    ],
    [
        "0399",
        "Aimere"
    ],
    [
        "0402",
        "Waiwerang"
    ],
    [
        "0556",
        "Pulau Palue"
    ],
    [
        "0557",
        "Reo"
    ],
    [
        "0558",
        "Solor"
    ],
    [
        "0187",
        "Atapupu"
    ],
    [
        "0559",
        "Mentigi"
    ],
    [
        "0560",
        "Sampalan"
    ],
    [
        "0561",
        "Tribuana Kusamba"
    ],
    [
        "0562",
        "Ulu Siau"
    ],
    [
        "0563",
        "Pelabuhan Sui Kunyit"
    ],
    [
        "0564",
        "Pulau Temajo"
    ],
    [
        "0563",
        "Sui Kunyit"
    ],
    [
        "0564",
        "Pulau Lemukutan"
    ],
    [
        "0563",
        "Teluk Suak"
    ],
    [
        "0565",
        "Sui Kunyit"
    ],
    [
        "0566",
        "Pulau Temajo"
    ],
    [
        "0567",
        "Pototano"
    ],
    [
        "0568",
        "Kayangan"
    ],
    [
        "0569",
        "Kupal"
    ],
    [
        "0570",
        "Busua"
    ],
    [
        "0571",
        "Indari"
    ],
    [
        "0169",
        "Nusa Penida"
    ],
    [
        "0173",
        "Bima, NTB"
    ]
    ];
